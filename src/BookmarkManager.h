// This file is part of the Bookmark Manager plugin for Notepad++.
// Copyright (C) 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef BOOKMARKMANAGER_H_
#define BOOKMARKMANAGER_H_

#include "PluginInterface.h"
#include <LosaXML/PersistentConfigItem.h>
#include <LosaXML/PersistentConfigBunch.h>
#include <LosaXML/DOMDocument.h>
#include <boost/shared_ptr.hpp>
#include <boost/optional.hpp>
#include <boost/scoped_ptr.hpp>
#include <vector>
#include <string>
#include <map>
#include <list>

namespace Npp
{
  namespace Plugin
  {
    class CPluginCommandList;
    typedef std::vector<unsigned char> CRGBAImage;

    enum class NView: int
    {
      Main = 0,
      Second = 1
    };

    // http://www.scintilla.org/ScintillaDoc.html
    class CScintilla
    {
      public:
        CScintilla(const HWND aHandle, const NView aView);

        HWND getHandle() const;
        NView getView() const;

        int getCurrentLine() const;
        void removeMarkers(int aMarkerNumber = -1);
        bool doesLineHaveMarker(const int aLineNumber, const int aMarkerNumber) const;
        bool addMarker(const int aLineNumber, const int aMarkerNumber, int & arMarkerHandle);
        void removeMarker(const int aLineNumber, const int aMarkerNumber);
        bool getLineFromMarker(const int aMarkerHandle, int & arLineNumber) const;
        int getLineFromPosition(const int aPosition) const;

        int getMarginSymbols(const int aMarginIndex) const;
        void setMarginSymbols(const int aMarginIndex, const int aMarkerMask);
        void setMarginType(const int aMarginIndex, const int aType);
        void setMarginWidth(const int aMarginIndex, const int aWidth);

        int getLineCount() const;
        void gotoLine(const int aLineNumber, const bool aCenterCaretVertically);
        bool isLineVisible(const int aLineNumber);

        void setCustomMarkerSymbol(const int aMarkerNumber, const unsigned aWidth, const unsigned aHeight,
          const CRGBAImage & aImage);

      private:
        HWND mHandle;
        NView mView;
    };

    struct CBookmarkSymbol
    {
      public:
        CBookmarkSymbol(unsigned aWidth, unsigned aHeight, CRGBAImage aRGBAImage);

        unsigned width;
        unsigned height;
        CRGBAImage rgbaImage;
    };

    struct CMarkerKey
    {
      public:
        CMarkerKey();
        CMarkerKey(const std::basic_string<TCHAR> & aFilename, const NView aView);

        bool operator() (const CMarkerKey & a, const CMarkerKey & b) const;

        const std::basic_string<TCHAR> & getFilename() const;
        const NView getView() const;

      private:
        std::basic_string<TCHAR> mFilename;
        NView mView;
    };

    class CBookmark
    {
      public:
        CBookmark(const UCHAR aCharacter);

        bool getMarkerNumber(const CMarkerKey & aKey, int & arMarkerNumber) const;
        void setMarkerNumber(const CMarkerKey & aKey, const int aValue);
        bool resetMarkerNumber(const CMarkerKey & aKey);
        void resetMarkerNumbers();

        UCHAR character;
        boost::optional<CBookmarkSymbol> symbol;

      private:
        typedef std::map<CMarkerKey, boost::optional<int>, CMarkerKey> CMarkerNumberMap;
        CMarkerNumberMap mMarkerNumberMap;
    };

    typedef std::list<const CBookmark *> CBookmarkList;

    struct CMarker
    {
      public:
        CMarker();
        CMarker(const int aHandle, const int aSymbolWidth);

        int handle;
        int symbolWidth;
    };

    class CBookmarkConfig : public LosaXML::CPersistentConfigItem
    {
      typedef LosaXML::CPersistentConfigItem inherited;

      public:
        CBookmarkConfig(CPersistentConfigItem * apParent);

        CBookmarkConfig(const CBookmarkConfig & aSource);
        CBookmarkConfig & operator= (const CBookmarkConfig & aSource);

        virtual void reset();

        NView getView() const;
        void setView(const NView aValue);

        const std::wstring & getFilename() const;
        void setFilename(const std::wstring & aValue);

        TCHAR getCharacter() const;
        void setCharacter(const TCHAR aValue);

        unsigned getLine() const;
        void setLine(const unsigned aValue);

      protected:
        virtual bool read(LosaXML::CDOMNode & arConfigNode, std::wstring & arErrorMessage);
        virtual bool write(LosaXML::CDOMNode & arConfigNode, std::wstring & arErrorMessage);

      private:
        NView mView;
        std::wstring mFilename;
        TCHAR mCharacter;
        unsigned mLine;
    };

    typedef LosaXML::CPersistentConfigBunchItemFactory<CBookmarkConfig> CBookmarkConfigBunchItemFactory;
    typedef std::list<CBookmarkConfig *> CBookmarkConfigList;

    class CBookmarkConfigBunch : public LosaXML::CPersistentConfigBunch<CBookmarkConfig>
    {
      typedef LosaXML::CPersistentConfigBunch<CBookmarkConfig> inherited;

      public:
        CBookmarkConfigBunch(CPersistentConfigItem * apParent, CBookmarkConfigBunchItemFactory & aItemFactory);

        bool get(const NView aView, const std::basic_string<TCHAR> & aFilename, CBookmarkConfigList & arItems) const;
        bool get(const NView aView, const std::basic_string<TCHAR> & aFilename, const TCHAR aCharacter,
          CBookmarkConfig * & aprItem) const;

        virtual bool extract(CBookmarkConfig * aprItem);

        bool add(CBookmarkConfig * apItem, std::wstring & arErrorMessage);
        bool remove(CBookmarkConfig * & aprItem);
    };

    class CBookmarkManagerConfig : public LosaXML::CPersistentConfigItem
    {
      typedef LosaXML::CPersistentConfigItem inherited;

      public:
        CBookmarkManagerConfig();

        CBookmarkManagerConfig(const CBookmarkManagerConfig & aSource);
        CBookmarkManagerConfig & operator= (const CBookmarkManagerConfig & aSource);

        CBookmarkConfigBunch & getBookmarks();

        virtual bool readFromFile(std::string aFilename, std::wstring & arErrorMessage) throw();
        virtual bool saveToFile(std::string aFilename, std::wstring & arErrorMessage) throw();

      private:
        CBookmarkConfigBunchItemFactory mBookmarksItemFactory;
        CBookmarkConfigBunch mBookmarks;
    };

    class CBookmarkManager
    {
      typedef std::map<UCHAR /* bookmark character */, boost::shared_ptr<CBookmark> > CBookmarkMap;
      typedef std::map<CMarkerKey, std::map<int /* marker number */, CMarker>, CMarkerKey> CMarkerMap;

      public:
        CBookmarkManager(const NppData & aNppData, CPluginCommandList & aPluginCommands);
        virtual ~CBookmarkManager();

        const boost::shared_ptr<CBookmark> addBookmark(const UCHAR aBookmarkCharacter);

        void addToggleCommand(const CBookmark & aBookmark, PFUNCPLUGINCMD apTrigger);
        void addGotoCommand(const CBookmark & aBookmark, PFUNCPLUGINCMD apTrigger);
        void addClearBookmarkOfFileCommand(PFUNCPLUGINCMD apTrigger);
        void addClearBookmarkOfAllFilesCommand(PFUNCPLUGINCMD apTrigger);

        void toggleBookmark(const UCHAR aBookmarkCharacter);
        void gotoBookmark(const UCHAR aBookmarkCharacter);
        void clearBookmarksOfCurrentFile();
        void clearBookmarksOfAllFiles();

        void textDeleted(const HWND aScintillaHandle, const int aPosition, const int aLinesAdded);
        void fileOpened(const std::basic_string<TCHAR> & aFilename);
        void fileBeforeClose(const HWND aScintillaHandle, const std::basic_string<TCHAR> & aFilename);
        void fileChanged(const std::basic_string<TCHAR> & aFilename);

        bool getFilenameFromBuffer(const HWND aNppHandle, const int aBufferID, std::basic_string<TCHAR> & arFilename) const;

      private:
        bool getCurrentScintilla(boost::scoped_ptr<CScintilla> & arCurrentScintilla) const;
        bool getScintillaFromHandle(const HWND aScintillaHandle, boost::scoped_ptr<CScintilla> & arCurrentScintilla) const;
        void setupBookmarkSymbolMargin(const CMarkerKey & aKey, CScintilla & aScintilla);

        bool getBookmark(const UCHAR aBookmarkCharacter, boost::shared_ptr<CBookmark> & arBookmark) const;
        bool getMarkerHandle(const CMarkerKey & aKey, const int aMarkerNumber, int & arMarkerHandle) const;
        bool getLineFromMarker(const CMarkerKey & aKey, const int aMarkerNumber, const CScintilla & aScintilla,
          int & arLineNumber) const;
        bool getBookmarkListFromMarker(const CMarkerKey & aKey, const int aMarkerNumber,
          CBookmarkList & arBookmarks) const;
        bool createMarkerSymbol(const CMarkerKey & aKey, const int aMarkerNumber, unsigned & aWidth,
          unsigned & aHeight, CRGBAImage & arBookmarkImage) const;
        void updateMarkerSymbol(const CMarkerKey & aKey, CScintilla & aScintilla, const int aMarkerNumber);
        bool addMarker(CBookmark & aBookmark, CScintilla & aScintilla, int aLineNumber,
          const CMarkerKey & aKey);
        bool removeMarker(const CMarkerKey & aKey, const int & aMarkerNumber, CBookmark & aBookmark,
          CScintilla & aScintilla, int & arMarkerLineNumber);

        std::string getConfigFilename() const;
        std::basic_string<TCHAR> getNppPluginDirectory() const;

        const NppData & mNppData;
        CPluginCommandList & mPluginCommands;

        const int mBookmarkSymbolMarginIndex;

        CBookmarkMap mBookmarkMap;
        CMarkerMap mMarkerMap;
        std::basic_string<TCHAR> mNppPluginDirectory;
        std::string mConfigFilename;
        CBookmarkManagerConfig mConfig;
    };
  }
}

#endif
