// This file is part of the Bookmark Manager plugin for Notepad++.
// Copyright (C) 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef PLUGINCOMMANDLIST_H_
#define PLUGINCOMMANDLIST_H_

#include "PluginInterface.h" // for FuncItem
#include <boost/shared_ptr.hpp>
#include <boost/optional.hpp>
#include <vector>
#include <string>

namespace Npp
{
  namespace Plugin
  {
    class CPluginCommandList
    {
      typedef std::vector<FuncItem> CItemList;
      typedef std::vector<boost::shared_ptr<ShortcutKey> > CShortcutList;

      public:
        CPluginCommandList();

        void clear();

        const std::size_t size() const;
        const FuncItem * getItems() const;

        void add(const std::basic_string<TCHAR> & aName, boost::optional<ShortcutKey> aShortcut,
          PFUNCPLUGINCMD aTrigger);

        CItemList mPluginCommands;
        CShortcutList mShortcuts;
    };
  }
}

#endif
