//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "PluginDefinition.h"
#include "PluginCommandList.h"
#include "BookmarkManager.h"

extern Npp::Plugin::CPluginCommandList pcl;
extern Npp::Plugin::CBookmarkManager * bm;
extern NppData nppData;


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  reasonForCall, 
                       LPVOID lpReserved )
{
  switch (reasonForCall)
  {
    case DLL_PROCESS_ATTACH:
      pluginInit(hModule);
      break;

    case DLL_PROCESS_DETACH:
      pluginCleanUp();
      break;

    case DLL_THREAD_ATTACH:
      break;

    case DLL_THREAD_DETACH:
      break;
  }

  return TRUE;
}


extern "C" __declspec(dllexport) void setInfo(NppData notpadPlusData)
{
  nppData = notpadPlusData;
  commandMenuInit();
}

extern "C" __declspec(dllexport) const TCHAR * getName()
{
  return NPP_PLUGIN_NAME;
}

extern "C" __declspec(dllexport) FuncItem * getFuncsArray(int *nbF)
{
  *nbF = pcl.size();

  return const_cast<FuncItem *>(pcl.getItems());
}

extern "C" __declspec(dllexport) void beNotified(SCNotification *notification)
{
  switch (notification->nmhdr.code)
  {
    case NPPN_SHUTDOWN:
    {
      commandMenuCleanUp();
      break;
    }

    case NPPN_FILEOPENED:
    case NPPN_FILEBEFORECLOSE:
    case NPPN_BUFFERACTIVATED:
    {
      std::basic_string<TCHAR> lFilename;

      if (bm->getFilenameFromBuffer((HWND)notification->nmhdr.hwndFrom, (int)notification->nmhdr.idFrom, lFilename))
      {
        if (notification->nmhdr.code == NPPN_FILEOPENED)
          bm->fileOpened(lFilename);        else
        if (notification->nmhdr.code == NPPN_FILEBEFORECLOSE)
          bm->fileBeforeClose((HWND)notification->nmhdr.hwndFrom, lFilename);
        else
          // NPPN_BUFFERACTIVATED
          bm->fileChanged(lFilename);
      }

      break;
    }

    case SCN_MODIFIED:
    {
      if ((notification->modificationType & SC_MOD_DELETETEXT) == SC_MOD_DELETETEXT)
        bm->textDeleted((HWND)notification->nmhdr.hwndFrom, notification->position, notification->linesAdded);

      break;
    }

    default:
      return;
  }
}


// Here you can process the Npp Messages 
// I will make the messages accessible little by little, according to the need of plugin development.
// Please let me know if you need to access to some messages :
// http://sourceforge.net/forum/forum.php?forum_id=482781
//
extern "C" __declspec(dllexport) LRESULT messageProc(UINT Message, WPARAM wParam, LPARAM lParam)
{/*
  if (Message == WM_MOVE)
  {
    ::MessageBox(NULL, "move", "", MB_OK);
  }
*/
  return TRUE;
}

#ifdef UNICODE
extern "C" __declspec(dllexport) BOOL isUnicode()
{
    return TRUE;
}
#endif //UNICODE
