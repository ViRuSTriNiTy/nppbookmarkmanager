// This file is part of the Bookmark Manager plugin for Notepad++.
// Copyright (C) 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef VERSION_H_
#define VERSION_H_

#define PROJECT_DISPLAY_NAME "@PROJECT_DISPLAY_NAME@"
#define PROJECT_VERSION "@PROJECT_VERSION_MAJOR@.@PROJECT_VERSION_MINOR@.0.@PROJECT_VERSION_BUILD@"
#define PROJECT_LEGAL_COPYRIGHT "@PROJECT_LEGAL_COPYRIGHT@"
#define PROJECT_COMMENTS "@PROJECT_COMMENTS@"

#endif
