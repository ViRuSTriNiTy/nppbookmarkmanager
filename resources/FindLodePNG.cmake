# - Try to find LodePNG
# Once done this will define
#
#  LodePNG_FOUND - system has LodePNG
#  LodePNG_INCLUDE_DIRS - the LodePNG include directories
#  LodePNG_LIBRARY_DIRS - the directories where the static libs of LodePNG were found
#  LodePNG_LIBRARIES -link these to use LodePNG
#  LodePNG_VERSION_STRING - the version of LodePNG found (since CMake 2.8.8)

#=============================================================================
# Copyright 2006-2012 Kitware, Inc.
# Copyright 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

set(LodePNG_INCLUDE_DIRS "$ENV{LODEPNG_SOURCES}")
set(LodePNG_LIBRARY_DIRS "$ENV{LODEPNG_LIB}")
set(LodePNG_LIBRARIES "LodePNG")

# handle the QUIETLY and REQUIRED arguments and set LodePNG_FOUND to TRUE if
# all listed variables are valid
include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LodePNG
  FOUND_VAR LodePNG_FOUND
  REQUIRED_VARS LodePNG_INCLUDE_DIRS LodePNG_LIBRARY_DIRS
  VERSION_VAR LodePNG_VERSION_STRING)