@echo off
IF EXIST bin\*.dll del bin\*.dll /Q
IF EXIST bin\NppBookmarkManager (
  rmdir /S /Q bin\NppBookmarkManager
)
IF EXIST obj\*.a del obj\*.a /Q
IF EXIST obj\Version.* del obj\Version.* /Q
IF EXIST CMakeFiles (
  rmdir /S /Q CMakeFiles
)
IF EXIST docs (
  rmdir /S /Q docs
)
IF EXIST cmake_install.cmake del cmake_install.cmake /Q
IF EXIST CMakeCache.txt del CMakeCache.txt
IF EXIST Makefile del Makefile
IF EXIST install_manifest.txt del install_manifest.txt